<?php
/**
 * @file
 * Displays the currently playing track from your Last.fm/Libre.fm account.
 */

/**
 * Implements hook_theme().
 */
function lastfm_now_playing_theme() {
  return array(
    'now_playing' => array(
      'variables' => array('track' => array()),
      'template' => 'now_playing',
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function lastfm_now_playing_block_info() {
  $blocks[0] = array(
    'info' => t('Last.fm now playing'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function lastfm_now_playing_block_configure($delta = '') {
  $form = array();

  switch ($delta) {
    case 0:

      // Settings form
      $form['lastfm_now_playing'] = array(
        '#type' => 'fieldset',
        '#title' => t('Last.fm/Libre.fm details'),
      );
      $form['lastfm_now_playing']['lastfm_now_playing_site'] = array(
        '#type' => 'radios',
        '#title' => t('Website'),
        '#description' => t('The site to display information from.'),
        '#options' => array('lastfm' => t('Last.fm'), 'librefm' => t('Libre.fm')),
        '#default_value' => variable_get('lastfm_now_playing_site', 'lastfm'),
      );
      $form['lastfm_now_playing']['lastfm_now_playing_user'] = array(
        '#type' => 'textfield',
        '#title' => t('User'),
        '#description' => t('Your Last.fm/Libre.fm username.'),
        '#default_value' => variable_get('lastfm_now_playing_user', NULL),
        '#required' => TRUE,
      );
      $form['lastfm_now_playing']['lastfm_now_playing_apikey'] = array(
        '#type' => 'textfield',
        '#title' => t('API key'),
        '#description' => t('Your Last.fm !api_key. Not required for Libre.fm.', array('!api_key' => l(t('API key'), 'http://www.last.fm/api/account', array('attributes' => array('title' => 'Get a Last.fm API key'))))),
        '#default_value' => variable_get('lastfm_now_playing_apikey', NULL),
        '#required' => FALSE,
        '#states' => array(
          // API key not required for Libre.fm
          'invisible' => array(
            ':input[name="lastfm_now_playing_site"]' => array('value' => 'librefm'),
          ),
        ),
      );

    break;
  }

  return $form;
}

/**
 * Implements hook_block_save().
 */
function lastfm_now_playing_block_save($delta = '', $edit = array()) {
  switch ($delta) {
    case 0:

      // Save settings as variables
      variable_set('lastfm_now_playing_site', $edit['lastfm_now_playing_site']);
      variable_set('lastfm_now_playing_user', $edit['lastfm_now_playing_user']);
      variable_set('lastfm_now_playing_apikey', $edit['lastfm_now_playing_apikey']);

    break;
  }
}

/**
 * Implements hook_block_view().
 */
function lastfm_now_playing_block_view($delta = '') {
  $block = array();

  switch ($delta) {
    case 0:
      $block['subject'] = t('Now playing');
      $block['content'] = array(
        '#theme' => 'now_playing',
        '#track' => lastfm_now_playing_track(),
        '#attached' => array(
          'css' => array(drupal_get_path('module', 'lastfm_now_playing') . '/lastfm_now_playing.css'),
        ),
      );
    break;
  }

  return $block;
}

/**
 * Get current track information from Last.fm/Libre.fm.
 */
function lastfm_now_playing_track() {
  $track = array();

  // Get Last.fm/Libre.fm settings
  $site = variable_get('lastfm_now_playing_site', 'lastfm');
  $user = variable_get('lastfm_now_playing_user', NULL);
  $apikey = variable_get('lastfm_now_playing_apikey', NULL);

  // Get most recent track
  if ($site == 'lastfm') {
    $track_url = "http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user={$user}&limit=1&api_key={$apikey}";
  }
  else {
    $track_url = "http://alpha.libre.fm/2.0/?method=user.getrecenttracks&user={$user}&limit=1";
  }
  $data = lastfm_now_playing_update($track_url);
  $track['error'] = $data['error'];
  $track['site'] = $site;

  if (empty($data['error'])) {
    // Populate the $track array with data
    $latest = $data->recenttracks->track;
    $track['now_playing'] = ($latest['nowplaying']) ? TRUE : FALSE;
    $track['name'] = (string) $latest->name;
    $track['artist'] = (string) $latest->artist;
    $track['album'] = (string) $latest->album;
    $track['url'] = (string) $latest->url;
    foreach ($latest->image as $image) {
      $size = (string) $image['size'];
      $track['image'][$size] = (string) $image;
    }
    $track['date'] = (integer) $latest->date['uts'];
  }

  // Get user info
  if ($site == 'lastfm') {
    $user_url = "http://ws.audioscrobbler.com/2.0/?method=user.getinfo&user={$user}&api_key={$apikey}";
  }
  else {
    $user_url = "http://alpha.libre.fm/2.0/?method=user.getinfo&user={$user}";
  }
  $info = lastfm_now_playing_update($user_url);

  // Add link to Last.fm/Libre.fm user profile to $track array
  if (empty($info['error'])) {
    $track['user'] = (string) $info->user->url;
  }

  return $track;
}

/**
 * Make an API call to Last.fm/Libre.fm using cURL.
 */
function lastfm_now_playing_update($url) {
  // Setup cURL
  $ch = curl_init($url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  // Get Last.fm/Libre.fm data
  $result = curl_exec($ch);

  // Check for cURL errors
  $error = curl_errno($ch);
  curl_close($ch);
  if ($error > 0) {
    return array(
      'error' => t('cURL error: @error', array('@error' => $error)),
    );
  }

  // Get XML data
  $data = simplexml_load_string($result);

  // Check for Last.fm/Libre.fm API errors
  if ($data['status'] == 'failed') {
    return array(
      'error' => $data->error,
    );
  }

  return $data;
}

